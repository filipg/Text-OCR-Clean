#!perl -T

use Test::More tests => 1;

BEGIN {
    use_ok( 'Text::OCR::Clean' ) || print "Bail out!\n";
}

diag( "Testing Text::OCR::Clean $Text::OCR::Clean::VERSION, Perl $], $^X" );
