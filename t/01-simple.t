#!perl -T

use utf8;

use Test::More tests => 3;

use Text::OCR::Clean qw(clean_ocr_output);

is(Text::OCR::Clean::clean_ocr_output(<<'EOI'),
Foo bar-
baz.
EOI
   "Foo barbaz.\n",
   "simple hyphen test");

is(Text::OCR::Clean::clean_ocr_output(<<'EOI'),
Oto przedstawiciel służącej interesom belweaer-
skiej lewicy Ajencji Wschodniej (AW.) poprosił p.
dra Nowaka o wyjaśnienie stanu sprawy Małopolski
wsch. w związku z autonomią i wyborami i — mię¬
dzy innemi — usłyszał następujące rewelacje:
EOI
   <<'EOO',
Oto przedstawiciel służącej interesom belweaerskiej lewicy Ajencji Wschodniej (AW.) poprosił p.
dra Nowaka o wyjaśnienie stanu sprawy Małopolski
wsch. w związku z autonomią i wyborami i — między innemi — usłyszał następujące rewelacje:
EOO
   "more complicated hyphen test");

is(Text::OCR::Clean::clean_ocr_output("po.\nkaz"),
   "pokaz",
   "dot as hyphen");



diag( "Testing Text::OCR::Clean $Text::OCR::Clean::VERSION, Perl $], $^X" );
