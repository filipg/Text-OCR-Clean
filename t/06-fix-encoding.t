#!perl -T

use utf8;

use Test::More tests => 1;

use Text::OCR::Clean qw(clean_ocr_output);

is(Text::OCR::Clean::clean_ocr_output(<<'EOI', "pl"),
i M³odzie¿y; „Rodzinne Czytanki” w Bibliotece; „Zielona Biblioteka”; „Festiwal poetów”; Turniej Jednego Wiersza; Tydzieñ Bibliotek; Ca³a Polska czyta dzieciom; „4 Pory Ksi¹¿ki”; Dyskusyjny Klub Ksi¹¿ki – to ogólnopolskie akcje, w których kêdzierzyñsko-kozielska biblioteka aktywnie uczestniczy.
Jubileusz sk³ania do podsumowañ, analiz, spogl¹dania wstecz,
porównañ z tym, co aktualne. Na przestrzeni lat Biblioteka
zmienia³a siê wraz z otoczeniem, mimo to pozosta³a placówk¹,
która pomaga swoim czytelnikom w nauce, pracy czy organizacji wolnego czasu.
Dzia³alnoœæ Biblioteki poddawana jest nieustannej spo³ecznej
ocenie, to mieszkañcy miasta, czytelnicy, a mo¿e ju¿ klienci,
korzystaj¹c z us³ug potwierdzaj¹ jej spo³eczn¹ u¿ytecznoœæ.
W manifeœcie Bibliotek publicznych UNESCO widnieje m.in.
taki zapis: „Gmach bêd¹cy pomieszczeniem biblioteki winien
mieæ po³o¿enie centralne, ³atwo dostêpne, równie¿ dla niepe³nosprawnych… jego wyposa¿enie powinno byæ estetyczne,
wygodne i przytulne”. Mamy to wszystko dziêki ¿yczliwoœci
i zrozumieniu roli Biblioteki w lokalnej spo³ecznoœci przez
w³adze samorz¹dowe.
Patrz¹c na przesz³oœæ Miejskiej Biblioteki Publicznej, jej aktualne dokonania, jestem spokojna o przysz³oœæ tej placówki. Mam
bowiem wra¿enie, ¿e zarówno zatrudnieni w niej ludzie, jak
i w³adze lokalne, a tak¿e nasi czytelnicy s¹ sobie wzajemnie potrzebni, tworz¹ bowiem coœ nieprzemijaj¹cego, „historiê ma³ej
ojczyzny”. I za to im dziêkujê.
I na koniec rocznicowe ¿yczenie – chcia³abym na nastêpny jubileusz 65-lecia wjechaæ na drugie piêtro Biblioteki wind¹.
Brakuje jej bowiem, nie dla bibliotekarzy, ale dla czytelników.
Helena Bulanda

–8–
EOI
<<EOO);
i Młodzieży; „Rodzinne Czytanki” w Bibliotece; „Zielona Biblioteka”; „Festiwal poetów”; Turniej Jednego Wiersza; Tydzień Bibliotek; Cała Polska czyta dzieciom; „4 Pory Książki”; Dyskusyjny Klub Książki – to ogólnopolskie akcje, w których kędzierzyńsko-kozielska biblioteka aktywnie uczestniczy.
Jubileusz skłania do podsumowań, analiz, spoglądania wstecz,
porównań z tym, co aktualne. Na przestrzeni lat Biblioteka
zmieniała się wraz z otoczeniem, mimo to pozostała placówką,
która pomaga swoim czytelnikom w nauce, pracy czy organizacji wolnego czasu.
Działalność Biblioteki poddawana jest nieustannej społecznej
ocenie, to mieszkańcy miasta, czytelnicy, a może już klienci,
korzystając z usług potwierdzają jej społeczną użyteczność.
W manifeście Bibliotek publicznych UNESCO widnieje m.in.
taki zapis: „Gmach będący pomieszczeniem biblioteki winien
mieć położenie centralne, łatwo dostępne, również dla niepełnosprawnych… jego wyposażenie powinno być estetyczne,
wygodne i przytulne”. Mamy to wszystko dzięki życzliwości
i zrozumieniu roli Biblioteki w lokalnej społeczności przez
władze samorządowe.
Patrząc na przeszłość Miejskiej Biblioteki Publicznej, jej aktualne dokonania, jestem spokojna o przyszłość tej placówki. Mam
bowiem wrażenie, że zarówno zatrudnieni w niej ludzie, jak
i władze lokalne, a także nasi czytelnicy są sobie wzajemnie potrzebni, tworzą bowiem coś nieprzemijającego, „historię małej
ojczyzny”. I za to im dziękuję.
I na koniec rocznicowe życzenie – chciałabym na następny jubileusz 65-lecia wjechać na drugie piętro Biblioteki windą.
Brakuje jej bowiem, nie dla bibliotekarzy, ale dla czytelników.
Helena Bulanda

–8–
EOO
