#!perl -T

use utf8;

use Test::More tests => 1;

use Text::OCR::Clean qw(clean_ocr_output);

is(Text::OCR::Clean::clean_ocr_output(<<'EOI'),
bardyzan: źIV, CMs. ~nie daw. «spis:
z baną osadzona na długim drzewcu, halabar-
da» ll [.
(d.-nm. bardosun ze irdwnłc. partosanz)
EOI
<<'EOO',
bardyzan: źIV, CMs. ~nie daw. «spis:
z baną osadzona na długim drzewcu, halabarda» ll [.
(d.-nm. bardosun ze irdwnłc. partosanz)
EOO
   "long hyphen test");
