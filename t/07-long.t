#!perl -T

use utf8;

use Test::More tests => 1;

use Text::OCR::Clean qw(clean_ocr_output);

is(Text::OCR::Clean::clean_ocr_output(<<'EOI'),
bcmzcha »: odm. jak źIII, lm D.›6w żart.
mb. «zakonnik -— bernardyn»: Najbardziej eks—
pansywni i towarzyscy byli bernardyni. Ru-
baszny banach: jest znamienną postacią daw-
nego świata szlacheckiego. BYm.Dzieje 1,352.
EOI
<<'EOO',
bcmzcha »: odm. jak źIII, lm D.›6w żart.
mb. «zakonnik -— bernardyn»: Najbardziej ekspansywni i towarzyscy byli bernardyni. Rubaszny banach: jest znamienną postacią dawnego świata szlacheckiego. BYm.Dzieje 1,352.
EOO
   "long hyphen test");
