#!perl -T

use utf8;

use Test::More tests => 1;

use Text::OCR::Clean qw(clean_ocr_output);

is(Text::OCR::Clean::clean_ocr_output(<<'EOI', 'pl'),
<<: rozelzll. . RONnoczes1ie w .Domu robotnletymodbylo Sll; zgromadzenie socjalno-demokriityezne, na ktOrem, mlçdzy innyml, przemawlal pose' Hybesz. Bomba. Stamllu'. Dtis1t'jlzel nocy rzuc-ono bombç na adlutanta I sufa tajneJ pollcll, Fth'na baslç w chwill gdy przeleidial przez przedmleåcle Pera. B:)mba wybuchh, ale nle wyrz1\dzÏla szkody. Aresztowano trzeeh Ormfan, podejrz ....ven 0 wykonanie z machu. Z parlamentu francuskiego. Pa..,:i. Seoat zatwlerdzll dodatkowy kredyt, potrzebny celem
EOI
<<'EOO',
<<: rozelzll. . RONnoczes1ie w .Domu robotnletymodbylo Sll; zgromadzenie socjalno-demokriityezne, na ktOrem, mlçdzy innyml, przemawlal pose' Hybesz. Bomba. Stamllu'. Dtis1t'jlzel nocy rzuc-ono bombç na adiutanta I sufa tajneJ pollcll, Fth'na baslç w chwill gdy przeleidial przez przedmleåcle Pera. B:)mba wybuchh, ale nle wyrz1\dzÏla szkody. Aresztowano trzeeh Ormfan, podejrz ....ven 0 wykonanie z machu. Z parlamentu francuskiego. Pa..,:i. Seoat zatwlerdzll dodatkowy kredyt, potrzebny celem
EOO
   "dictionary test");
