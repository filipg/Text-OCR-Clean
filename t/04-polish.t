#!perl -T

use utf8;

use Test::More tests => 13;

use Text::OCR::Clean qw(clean_ocr_output);

is(Text::OCR::Clean::clean_ocr_output("ratyfikujqcq", "pl"), "ratyfikującą");

is(Text::OCR::Clean::clean_ocr_output("ratyfikujqcq", "en"), "ratyfikujqcq");

is(Text::OCR::Clean::clean_ocr_output("ratyfikujqcq"), "ratyfikujqcq");

is(Text::OCR::Clean::clean_ocr_output("quiz", "pl"), "quiz");

is(Text::OCR::Clean::clean_ocr_output("maque", "pl"), "maque");

is(Text::OCR::Clean::clean_ocr_output("Velazquez", "pl"), "Velazquez");

is(Text::OCR::Clean::clean_ocr_output("zwierzqt", "pl"), "zwierząt");

is(Text::OCR::Clean::clean_ocr_output("wchodzqce", "pl"), "wchodzące");

is(Text::OCR::Clean::clean_ocr_output("naukowq", "pl"), "naukową");

is(Text::OCR::Clean::clean_ocr_output("oboz6w", "pl"), "obozów");

is(Text::OCR::Clean::clean_ocr_output("radosci", "pl"), "radości");

is(Text::OCR::Clean::clean_ocr_output("radosci życzę", "pl"), "radości życzę");

is(Text::OCR::Clean::clean_ocr_output("radosć", "pl"), "radość");
