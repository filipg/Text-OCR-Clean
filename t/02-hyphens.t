#!perl -T

use utf8;

use Test::More tests => 1;

use Text::OCR::Clean qw(clean_ocr_output);

is(Text::OCR::Clean::clean_ocr_output(<<'EOI'),
kowskiego pałacu w naj­
świetniejszym - jak do­
tąd - polskim dramacie,
poświęconym Warszawie.
Ale nie Łazienki tylko
stanowią akcję "Nocy Li-
stopadowej". Także i
Krakowskie Przedmieś-
cie, teatr .. Rozmaitości"
rog Koziej, aleje Uja­
zdowskie ... Ow "warszaw­
EOI
   <<'EOO',
kowskiego pałacu w najświetniejszym - jak dotąd - polskim dramacie,
poświęconym Warszawie.
Ale nie Łazienki tylko
stanowią akcję "Nocy Listopadowej". Także i
Krakowskie Przedmieście, teatr .. Rozmaitości"
rog Koziej, aleje Ujazdowskie ... Ow "warszaw­
EOO
   "test with two types of hyphens");


diag( "Testing Text::OCR::Clean $Text::OCR::Clean::VERSION, Perl $], $^X" );
