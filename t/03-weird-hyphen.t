#!perl -T

use utf8;

use Test::More tests => 2;

use Text::OCR::Clean qw(clean_ocr_output);

is(Text::OCR::Clean::clean_ocr_output(
        "szybkiem wykonywaniem wszel¬\nkich uchwał i poleceń"),
        'szybkiem wykonywaniem wszelkich uchwał i poleceń');

is(Text::OCR::Clean::clean_ocr_output(
        "Innymi słowy odpowiedź Kościoła na py¬ tanie o jego miejsce we współczesnym świecie musi bazować na jego — Kościoła — samoświadomości, na dogmatycznej nauce o Kościele, na ekle¬ zjologii."),
        "Innymi słowy odpowiedź Kościoła na pytanie o jego miejsce we współczesnym świecie musi bazować na jego — Kościoła — samoświadomości, na dogmatycznej nauce o Kościele, na eklezjologii.");


diag( "Testing Text::OCR::Clean $Text::OCR::Clean::VERSION, Perl $], $^X" );
