#!perl -T

use utf8;

use Test::More tests => 1;

use Text::OCR::Clean qw(clean_ocr_output);

is(Text::OCR::Clean::clean_ocr_output(<<'EOI', "pl"),
..ych cdoaków israelickich reprezentacji miasta, ale nadaremnie... Zalatwiono wresacie skromny porzl}dek dzienny, dokonujl}c wyboru 12 cllonków do Rady nadaorcaej miejakiegoMuzeum praemyslowego i uchwalajlj,c datek 50 aIr. na pogorlelców Kolaczyc, dalej subwencj w kwocie 1000 zlr. i 10 stosów draewa dla lakladu g uchoniemych, I wreszcie celem regulacji ul. na Rurach zakupno po 6 Ilr. 28 sl!:!ni gruntu od p. Poludniewskiego - a wyjl}tkowe aapelnienie galeryj by to jesacae cillgle lagadk
EOI
<<EOO);
..ych cdoaków israelickich reprezentacji miasta, ale nadaremnie... Załatwiono wresacie skromny porządek dzienny, dokonując wyboru 12 cllonków do Rady nadaorcaej miejakiegoMuzeum praemyslowego i uchwalajlj,c datek 50 aIr. na pogorlelców Kołaczyc, dalej subwencj w kwocie 1000 zlr. i 10 stosów draewa dla lakladu g uchoniemych, I wreszcie celem regulacji ul. na Rurach zakupno po 6 Ilr. 28 sl!:!ni gruntu od p. Poludniewskiego - a wyjątkowe aapelnienie galeryj by to jesacae cillgle lagadk
EOO
