#!/usr/bin/perl

use strict;

use Text::OCR::Clean;

binmode(STDIN, ':utf8');
binmode(STDOUT, ':utf8');

while (my $line=<STDIN>) {
    chomp $line;

    $line = Text::OCR::Clean::clean_ocr_output($line, 'pl');

    print $line, "\n";
}
