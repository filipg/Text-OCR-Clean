Perl module for cleaning OCR-ed texts.

Requirements
------------

You may need to install some Perl modules, e.g.:

    sudo cpan install Text::Ligature

Installation
------------

    perl Makefile.PL
    make
    make test
    sudo make install

Using
-----

    ./clean.pl < file-to-cleaned.txt > cleaned-file.txt

Licence
-------

COPYRIGHT Filip Graliński 2012-2018
PROPRIETARY LICENSE
