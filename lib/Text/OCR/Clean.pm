package Text::OCR::Clean;

use 5.006;
use strict;
use warnings;

use utf8;

our @EXPORT_OK   = qw< clean_ocr_output >;
our %EXPORT_TAGS = ( all => \@EXPORT_OK );

use Text::Ligature qw(from_ligatures);
use IO::Zlib;
use File::ShareDir ':ALL';
use Encode;

=head1 NAME

Text::OCR::Clean - Cleans an OCR output

=head1 VERSION

Version 0.03

=cut

our $VERSION = '0.03';


=head1 SYNOPSIS

    use Text::OCR::Clean qw(clean_ocr_output)

    # an OCR output (e.g. from Tesseract)
    my $ocr_output = ...

    my $foo = clean_ocr_output($ocr_output);
    ...


=head1 SUBROUTINES/METHODS

=head2 clean_ocr_output

=cut

sub clean_ocr_output {
    my ($ocr_output, $lang_code) = @_;

    $ocr_output =~ s/[-­¬]\s*\r?\n(?:)?([\p{Ll}])/$1/g;
    $ocr_output =~ s/([\p{Ll}]{2})— ?\r?\n(?:)?([\p{Ll}])/$1$2/g;

    # sometimes we don't require a newline
    $ocr_output =~ s/¬\s*\r?\n?([\p{L}])/$1/g;
    $ocr_output =~ s/[\001-\007\010\013\014\016-\037]/ /g;

    # sometimes a hyphen is misrecognised a dot, but you
    # must be careful about abbreviations
    $ocr_output =~ s/(\p{Ll}{5}|[aąeęiouy])\.\s*\r?\n([\p{Ll}])/$1$2/g;

    $ocr_output = from_ligatures($ocr_output);

    if (defined($lang_code) && $lang_code eq 'pl') {
        $ocr_output =~ s{([jc])q}{${1}ą}g;
        $ocr_output =~ s{zq([bcćdgkłst]|\b)}{zą${1}}g;
        $ocr_output =~ s{owq\b}{ową}g;
        $ocr_output =~ s{([bcćdfgjklłmnpstwzźż])6w\b}{${1}ów}g;
        $ocr_output =~ s{osć\b}{ość}g;
        $ocr_output =~ s{osci\b}{ości}g;

        $ocr_output =~ s<jl[}\\]><ją>g;
        $ocr_output =~ s<l}(\p{Ll})><ą$1>g;

        $ocr_output =~ s{siQ}{się}g;

        if ($ocr_output !~ m{[ąćęłńźżĄĆĘŁŃŚŹŻ]}) {
            $ocr_output =~ y/¹æê³ñœŸ¿Œ¯¶±/ąćęłńśźżŚŻśą/;
        }

        my $r_pl_fixes = get_pl_fixes();

        $ocr_output =~ s/[\p{L}-]+/exists $r_pl_fixes->{$&} ? $r_pl_fixes->{$&} : $&/ge;
    }

    return $ocr_output;
}

{
    my %pl_fixes;
    my $fh = IO::Zlib->new(module_file('Text::OCR::Clean', 'dictionary.tsv.gz'), "rb");
    while (my $line=<$fh>) {
        $line = decode('utf-8', $line);
        chomp $line;
        my ($var, $correct) = split/\t/,$line;
        $pl_fixes{$var} = $correct;
    }

    sub get_pl_fixes {
        return \%pl_fixes;
    }
}

=head1 DESCRIPTION

Cleans an OCR output in a simple manner. No external tools (e.g.
spellcheckers are used).

It assumes that you are interested only in the text itself. Structure
is not preserved.

Tested mostly on Polish books and newspapers OCR-ed with Tesseract.

=head1 AUTHOR

Filip Gralinski, C<< <filipg at ceti.pl> >>

=head1 BUGS

Please report any bugs or feature requests to C<bug-text-ocr-clean at rt.cpan.org>, or through
the web interface at L<http://rt.cpan.org/NoAuth/ReportBug.html?Queue=Text-OCR-Clean>.  I will be notified, and then you'll
automatically be notified of progress on your bug as I make changes.




=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc Text::OCR::Clean


You can also look for information at:

=over 4

=item * RT: CPAN's request tracker (report bugs here)

L<http://rt.cpan.org/NoAuth/Bugs.html?Dist=Text-OCR-Clean>

=item * AnnoCPAN: Annotated CPAN documentation

L<http://annocpan.org/dist/Text-OCR-Clean>

=item * CPAN Ratings

L<http://cpanratings.perl.org/d/Text-OCR-Clean>

=item * Search CPAN

L<http://search.cpan.org/dist/Text-OCR-Clean/>

=back


=head1 ACKNOWLEDGEMENTS


=head1 LICENSE AND COPYRIGHT

Copyright 2014-2018 Filip Gralinski.

This program is free software; you can redistribute it and/or modify it
under the terms of either: the GNU General Public License as published
by the Free Software Foundation; or the Artistic License.

See http://dev.perl.org/licenses/ for more information.


=cut

1; # End of Text::OCR::Clean
